(function () {
'use strict';

    /**
     * ...
     */
    angular.module('youplaylist.playlist')
        .run(appRun);
    
    //-------------------------------------------------------------------------
    /* @ngInject */

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'playlist',
                config: {
                    templateUrl: 'app/content/playlist/playlist.html',
                    url: '/playlist/:id',
                    controller: 'PlaylistController',
                    controllerAs: 'vm'
                }
            }
       ];
    }
})();