'use strict';

var config        = require('../config'),
    gulp          = require('gulp'),
    argv          = require('yargs').argv,
    preprocess    = require('gulp-preprocess'),
    handleErrors  = require('../util/handleErrors'),
    pngquant      = require('imagemin-pngquant'),
    runSequence   = require('run-sequence'),
    replace       = require('gulp-replace'),
    stripDebug    = require('gulp-strip-debug'),
    plugins       = require('gulp-load-plugins')({
      pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
    }),
    templateCache = require('gulp-angular-templatecache'),
    wiredep       = require('wiredep').stream;


// Setup environment variable for build. Define on command line, eg  "gulp build --env=blah"
// Fallback option is prod
var env = argv.env || 'prod';

  gulp.task('partials', function () {
    return gulp.src([
      config.src.root + '/app/**/*.html',
      config.tmp.root + '/serve/app/**/*.html'
    ])
      .pipe(plugins.minifyHtml({
        empty: true,
        spare: true,
        quotes: true
      }))
      .pipe(plugins.angularTemplatecache('templateCacheHtml.js', {
        module: config.app.module,
        root: 'app'
      }))
      .pipe(gulp.dest(config.tmp.root + '/partials/'));
  });

  gulp.task('html', ['inject', 'partials'], function () {
    var partialsInjectFile = gulp.src(config.tmp.root + '/partials/templateCacheHtml.js', { read: false });
    var partialsInjectOptions = {
      starttag: '<!-- inject:partials -->',
      ignorePath: config.tmp.root + '/partials',
      addRootSlash: false
    };

    var htmlFilter = plugins.filter('*.html', {restore: true});
    var jsFilter = plugins.filter('**/*.js', {restore: true});
    var cssFilter = plugins.filter('**/*.css', {restore: true});
    var assets;

    return gulp.src(config.tmp.root + '/serve/*.html')
      .pipe(plugins.inject(partialsInjectFile, partialsInjectOptions))
      
      .pipe(assets = plugins.useref.assets())
      .pipe(plugins.rev())
      .pipe(jsFilter)
      .pipe(plugins.ngAnnotate())
      .pipe(preprocess({context: { ENV: env}}))
      // Removes debugger statements
      .pipe(replace(/db\.[a-z]+\((.*)[\);|\)]/g, ''))
      // Removes console.log statements
      .pipe(stripDebug())
      .pipe(plugins.uglify({ preserveComments: plugins.uglifySaveLicense })).on('error', handleErrors)
      .pipe(jsFilter.restore)
      .pipe(cssFilter)
      .pipe(plugins.csso())
      .pipe(cssFilter.restore)
      .pipe(assets.restore())
      .pipe(plugins.useref())
      .pipe(plugins.revReplace())
      .pipe(htmlFilter)
      .pipe(plugins.minifyHtml({
        empty: true,
        spare: true,
        quotes: true,
        conditionals: true
      }))
      .pipe(htmlFilter.restore)
      .pipe(gulp.dest(config.dist.root + '/'))
      .pipe(plugins.size({ title: config.dist.root + '/', showFiles: true }));
  });

  gulp.task('fonts', function () {
     return gulp.src(config.fonts.src + '/**/*')
        .pipe(gulp.dest(config.dist.root+'/assets/fonts/'));
  });

  /* Dist image optimization */
  gulp.task('images', function() {
    return gulp.src(config.images.src  + '/*')
      .pipe(plugins.imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
      }))
      .pipe(gulp.dest(config.dist.root+'/assets/images/'));
  });

  /* Total dist cleaning */
  gulp.task('clean-before', function() {
    return gulp.src([config.dist.root, config.tmp.root] )
      .pipe(plugins.clean({ force: true }));
  });

  /* After-build cleaning to remove manifests */
  gulp.task('clean-after', function() {
    return gulp.src([config.dist.root + '/rev-manifest-*'])
      .pipe(plugins.clean());
  });

  gulp.task('build', function () {
     runSequence('clean-before', ['html'], ['images', 'fonts'], 'clean-after');
  });
