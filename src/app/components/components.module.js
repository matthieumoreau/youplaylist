(function () {
    'use strict';
    /**
     *   @class Components
     */
    angular.module('youplaylist.components', [
    	'youplaylist.components.ypvideo',
    	'youplaylist.components.yptags'

    ]);

})();
