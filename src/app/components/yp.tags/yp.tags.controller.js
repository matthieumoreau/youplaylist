(function () {
    'use strict';

    angular.module('youplaylist.components.yptags', [])
        .directive('ypTags', ypTags)
        .controller('TagsController', TagsController);


    function ypTags() {

        var directive = {
            restrict        : 'E',
            replace         : true,
            scope       : {
                video: '=',
                src: '=', 
                type: '@'
            },
            templateUrl: 'app/components/yp.tags/yp.tags.html',
            controller  : 'TagsController as vm',
            link  : tagsLink
        };


        return directive;

        function tagsLink(scope, element, attribute) {
        }
    }

    function TagsController($scope, $http, $location, $anchorScroll, $stateParams, $timeout, $localStorage){

        var vm = this;
        vm.tags = $localStorage.tags;
        $scope.tag = null;
        vm.removeTag = removeTag;
        vm.addTags = addTags; 
        $scope.disable = disable;

        function disable () {
            console.log($.inArray($scope.selectedTag, $scope.video.tags));
            if($.inArray($scope.selectedTag, $scope.video.tags) > -1) {
                return $scope.disableInput = true;
            } else { 
                return $scope.disableInput = false;
            }

        }

        
        // Ajouter un tag à une video 
        function addTags () {
            console.log("test");
            var selectedTag = $scope.selectedTag.toLowerCase();

            vm.videoTags = $scope.video.tags;
            vm.videoTags.push(selectedTag);

            if (vm.tags.indexOf( selectedTag ) == -1) {
                vm.tags.push(selectedTag);
            }; 

            $scope.selectedTag = null;
        }


        // Supprimer un tag à une video 
        function removeTag (tag) {
            tag = tag.toLowerCase();
            
            vm.videoTags = $scope.video.tags;

            var indexVideo = arrayObjectIndexOf(vm.videoTags, tag); 
            vm.videoTags.splice(indexVideo, 1);
        };




        function arrayObjectIndexOf(myArray, searchTerm) {
            for(var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i] === searchTerm) return i;
            }
            return -1;
        }


        

    }

})();
