'use strict';

var config      = require('../config');
var gulp        = require('gulp');
var browserSync = require('browser-sync');
var handleErrors = require('../util/handleErrors');
var plugins     = require('gulp-load-plugins')();


  gulp.task('scripts', function () {
    return gulp.src([config.src.app + '/**/*.js'])
      .pipe(plugins.jshint())
      .pipe(plugins.jshint.reporter('jshint-stylish'))
      .pipe(browserSync.reload({ stream: true }))
      .pipe(plugins.size())
  });