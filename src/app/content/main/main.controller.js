(function () {
    'use strict';

    angular.module('youplaylist.main')
        .controller('MainController', MainController);

        function MainController ($scope, $element, $timeout, $http, $window, $localStorage, TAGS) {

            var vm = this;
            // Ajouter une première liste de tags au localStorage
            $localStorage.tags = TAGS;
            // Initialiser le localStorage des playlists
            if ($localStorage.playlists == 0 || !$localStorage.playlists ) {
                $localStorage.playlists = [];
            }

            if ($localStorage.videos == 0 || !$localStorage.videos ) {
                $localStorage.videos = [];
            }
            
            vm.playlists = $localStorage.playlists;
            vm.filter = {$: undefined};
            vm.setFilter = setFilter;
            vm.addPlaylist = addPlaylist;
            $scope.removePlaylist = removePlaylist;

            // Supprimer une playlist
            function removePlaylist (playlist) {
                var index = arrayObjectIndexOf(vm.playlists, playlist.id, "id"); // 1
                vm.playlists.splice(index, 1);
            };

            // Ajouter une playlist
            function addPlaylist () {
                // Si ce n'est pas la première playlist
            	if ($localStorage.playlists.length > 0) {  
            		vm.playlists = $localStorage.playlists;
            		var lastPlaylist = vm.playlists[vm.playlists.length - 1];
                    var new_id = parseInt(lastPlaylist.id)+1;
            		var playlist = {
            			id:  new_id,
                        title: "Playlist "+new_id,
                        videos: [],
                        tags: []
            		};

            		vm.playlists.push(playlist);
            	   $window.location.href = '/playlist/'+new_id;

                // Si c'est la première playlist
            	} else {
            		var playlist = {
        				id: 1,
                        title: "Playlist 1",
                        videos: [],
                        tags: []
            		};
            		vm.playlists.push(playlist);
            		$window.location.href = '/playlist/1';
            	}
            }

            // Filter par tag
            function setFilter (){
                $scope.filter = {};
                $scope.filter[ "title" || '$'] = $scope.searchText;
            };


            function arrayObjectIndexOf(myArray, searchTerm, property) {
                for(var i = 0, len = myArray.length; i < len; i++) {
                    if (myArray[i][property] === searchTerm) return i;
                }
                return -1;
            }

        }
})();