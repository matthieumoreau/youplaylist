'use strict';

module.exports = {

  'browserPort'  : 3000,
  'UIPort'       : 3001,
  'serverPort'   : 3002,

  'api': {
  	'url'	: '',
    'port'  : 8000, 
    'root'  : 'api/public',
    'src'   : 'api/**/*.php'
  },

  // ROOTS 

  'src': {
    'root'  	: 'src',
    'app'		: 'src/app',
    'assets'	: 'src/assets'
  }, 

  'dist': {
    'root'  : 'build'
  },

  'tmp': {
    'root'  : '.tmp',
    'serve'	: '.tmp/serve'
  },

  // STYLES 

  'styles': {
  	'root': 'src/assets/styles',
    'src' : 'src/**/*.scss',
    'dest': 'build/css',
    'prodSourcemap': false,
    'sassIncludePaths': []
  },

  // SCRIPTS 

  'scripts': {
    'src' : 'src/app/**/*.js',
    'dest': 'build/js'
  },



  'images': {
    'src' : 'src/assets/images/**/*',
    'dest': 'build/images'
  },

  'fonts': {
    'src' : ['src/assets/fonts/**/*'],
    'dest': 'build/fonts'
  },

  'views': {
    'watch': [
      'index.html',
      'src/app/**/*.html'
    ],
    'src': 'src/**/*.html',
    'dest': 'app/js'
  },

  'gzip': {
    'src': 'build/**/*.{html,xml,json,css,js,js.map,css.map}',
    'dest': 'build/',
    'options': {}
  },


  'wiredep': {
    'directory': 'bower_components',
    'exclude': ['bootstrap-sass']
  },

  // ANGULAR JS 

  'app': {
  	'module' : 'youplaylist',
  }

};
