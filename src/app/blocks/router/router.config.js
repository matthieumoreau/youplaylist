(function () {

'use strict';

angular.module('blocks.router')
  .run(appRun);

  /* @ngInject */
  function appRun(routerHelper) {      
      routerHelper.configureStates([], '/');
  }

})();