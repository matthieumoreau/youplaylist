(function () {
    'use strict';

    angular.module('youplaylist.components.ypvideo', [])
        .directive('ypVideo', ypVideo)
        .controller('VideoController', VideoController);


    function ypVideo() {

        var directive = {
            restrict        : 'E',
            replace         : true,
            scope       : {
                src: '='
            },
            templateUrl: 'app/components/yp.video/yp.video.html',
            controller  : 'VideoController as vm',
            link  : videoLink
        };


        return directive;

        function videoLink(scope, element, attribute) {
        }
    }

    function VideoController($scope, $http, $location, $anchorScroll, $stateParams, $timeout){

        var vm = this;

        $scope.doSearch = doSearch;
        $scope.selectVideo = selectVideo;
        $scope.addVideo = addVideo;

        function selectVideo (index, video, show) {

            // Ajouter une vidéo de Youtube à la library  
            if (show == true ) {
                if ($scope.src.length == 0) {
                    var id = 1;
                } else {
                    var id = $scope.src.length+1;
                }

                $scope.selectedVideo = {
                    id: id,
                    id_yt: video.id.videoId,
                    title: video.snippet.title,
                    thumbnails: video.snippet.thumbnails,
                    tags: []
                }
            } else {
                $scope.selectedVideo = null; 


            }
        };

        function addVideo (video, show) {
            $scope.src.push(video);
            $scope.selectedVideo = null;
            return show = false;
        }




        // Rechercher une vidéo sur YouTube
        function doSearch () {
            
            var url = 'https://www.googleapis.com/youtube/v3/search?'
              + [
                'key=AIzaSyCWAtDmTmGfMJMv8jVj2Jb255NmaA7NM2A',
                'q=' + encodeURIComponent($scope.query),
                'part=snippet',
                'order=date',
                'maxResults=20', 
                'type=video', 
                'regionCode=FR'
              ].join('&');

            $http.jsonp(url, {params: {callback: 'JSON_CALLBACK'}}).success(function(data) {
                $scope.results = data.items;
                var copyResult = JSON.parse(JSON.stringify($scope.results));

                //debugger;

                if ( $scope.src.length > 0) {
                    var idYT = [];
                    var inPlaylistArr = [];

                    for (var i = 0; i < $scope.src.length; i++) {
                        idYT.push($scope.src[i].id_yt);
                    }
                   
                    for (var i = 0; i < copyResult.length; i++) {

                        if (idYT.indexOf($scope.results[i].id.videoId) > -1) {
                                var inPlaylist = true;
                        } else {
                                var inPlaylist = false;
                        }

                        copyResult[i]['inPlaylist'] = inPlaylist; 
                    };


                } else {

                    for (var i = 0; i < copyResult.length; i++) {

                        copyResult[i]['inPlaylist'] = false; 
                    }; 
                }

                $scope.videosFromYouTube = copyResult;
            });
        }


        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for(var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm) return i;
            }
            return -1;
        }

    }

})();
