'use strict';

var config        = require('../config'),
    gulp          = require('gulp'),
    autoprefixer  = require('autoprefixer'),
    handleErrors  = require('../util/handleErrors'),
    plugins       = require('gulp-load-plugins')(),
    templateCache = require('gulp-angular-templatecache'),
    wiredep       = require('wiredep').stream;

gulp.task('inject', ['scripts', 'styles', 'templateCache'], function () {

  var injectStyles = gulp.src([
    config.tmp.serve + '/app/**/*.css',
    '!' + config.tmp.serve + '/app/vendor.css'
  ], { read: false });

  var injectScripts = gulp.src([
    config.src.app + '/**/*.js',
    '!' + config.src.app + '/**/*.spec.js',
    '!' + config.src.app + '/**/*.mock.js'
  ])
  .pipe(plugins.angularFilesort())
  	.on('error', handleErrors);

  var injectOptions = {
    ignorePath: [config.src.root, config.tmp.serve],
    addRootSlash: false
  };

  return gulp.src(config.src.root + '/*.html')
    .pipe(plugins.inject(injectStyles, injectOptions))
    .pipe(plugins.inject(injectScripts, injectOptions))
    .pipe(wiredep(config.wiredep)).on('error', handleErrors)
    .pipe(gulp.dest(config.tmp.serve ));

});

gulp.task('templateCache', function () {

  var TEMPLATE_HEADER = '(function () {"use strict"; angular.module("<%= module %>"<%= standalone %>).run(["$templateCache", function($templateCache) {';
  var TEMPLATE_FOOTER = '}]);})();';
  return gulp.src([
    config.src.app + '/**/*.html'
    ])
    .pipe(templateCache({
      filename: 'templateCache.js',
      module: config.app.module,
      templateHeader: TEMPLATE_HEADER,
      templateFooter: TEMPLATE_FOOTER
    }))
    .pipe(gulp.dest(config.src.app + '/core'))
})
