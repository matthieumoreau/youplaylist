(function () {
    'use strict';

    angular.module('youplaylist.content', [
        'youplaylist.main',
        'youplaylist.videos',
        'youplaylist.playlist'
    ]);
})();