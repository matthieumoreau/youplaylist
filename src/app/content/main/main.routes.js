(function () {
'use strict';

    /**
     * ...
     */
    angular.module('youplaylist.main')
        .run(appRun);
    
    //-------------------------------------------------------------------------
    /* @ngInject */

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'main',
                config: {
                    templateUrl: 'app/content/main/main.html',
                    url: '/',
                    controller: 'MainController',
                    controllerAs: 'vm'
                }
            }
       ];
    }
})();