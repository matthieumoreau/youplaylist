(function () {
    'use strict';

    angular.module('youplaylist.playlist')
        .controller('PlaylistController', PlaylistController);

        function PlaylistController ($scope, $element, $timeout, $http, $localStorage, $state, TAGS) {

            var vm = this;
            vm.tags = $localStorage.tags;
            vm.videos = $localStorage.videos;
            vm.playlists = $localStorage.playlists;
            vm.playlist = vm.playlists[$state.params.id - 1];

            $scope.tag = tag;
            $scope.titlePlaylist = vm.playlist.title;

            $scope.createPlaylist =createPlaylist;
            $scope.updatePlaylist = updatePlaylist;

            $scope.removeVideofromList = removeVideofromList;
            $scope.removeVideofromPlaylist = removeVideofromPlaylist;  
            $scope.removeTag = removeTag;           

			// Filter par tag
			function tag (message) {
			    if ($scope.tags) {
			     	return $scope.tags.replace(/\s*,\s*/g, ',').split(',').every(function(tag) {
			        	return $.inArray(tag, message.tags) > -1;
			      	});
			    }
			    else {
			      return true;
			    }
			 };

			 // Filtrer si la video est déjà présente dans la playlist
			 $scope.inPlaylist = function (message) {

			 	var copyLibraryFilteredItems = JSON.parse(JSON.stringify(message));

			 	if ( vm.playlist.videos.length > 0) {
                    var idYT = [];

                    for (var i = 0; i < vm.playlist.videos.length; i++) {
                        idYT.push(vm.playlist.videos[i].id_yt);
                    }
                   	
                    if (idYT.indexOf(message.id_yt) > -1) {
                    	//return false;
                    } else {
                    	return true;
                    }
       
                } else {

                   	return true; 
                   
                }
                


			}

			 // Créer
			 function createPlaylist () {
			 	console.log(vm.playlist.title, $scope.titlePlaylist);

			 	vm.playlist.title = $scope.titlePlaylist; 

		 		if ($scope.tags) {
		 			var tags = $scope.tags.replace(/\s*,\s*/g, ',').split(',');
		 			tags.some(function(tag) {
		 				tag = tag.toLowerCase();
			 			vm.playlist.tags.push(tag);
			 		});
		 		}
			 	

			 	$scope.filteredItems.some(function(video) {
			 		vm.playlist.videos.push(video);
			 	});


			 }

			// Mettre à jour
			function updatePlaylist () {
			 	
			 	vm.playlist.title = $scope.titlePlaylist; 

			 	if ($scope.tags) {
		 			var tags = $scope.tags.replace(/\s*,\s*/g, ',').split(',');
		 			tags.some(function(tag) {
		 				tag = tag.toLowerCase();
			 			vm.playlist.tags.push(tag);
			 		});
		 		}

			 	// Update videos
			 	$scope.filteredItems.some(function(video) {
			 		vm.playlist.videos.push(video);
			 	});

			}

			//Supprimer une video de la playlist
			function removeVideofromList (video) {
                var index = arrayObjectObjectIndexOf($scope.filteredItems, video.id, "id"); // 1
                $scope.filteredItems.splice(index, 1);
	        };

	        //Supprimer une video de la playlist
			function removeVideofromPlaylist (video) {
                var index = arrayObjectObjectIndexOf(vm.playlist.videos, video.id, "id"); // 1
                vm.playlist.videos.splice(index, 1);
	        };

	        // Supprimer un tag de la playlist 
	        function removeTag (tag) {
	            tag = tag.toLowerCase();
	            
	            var indexTag = arrayObjectIndexOf(vm.playlist.tags, tag); 
	            vm.playlist.tags.splice(indexTag, 1);
	        };

		    function arrayObjectIndexOf(myArray, searchTerm) {
	            for(var i = 0, len = myArray.length; i < len; i++) {
	                if (myArray[i] === searchTerm) return i;
	            }
	            return -1;
	        }

	        function arrayObjectObjectIndexOf(myArray, searchTerm, property) {
			    for(var i = 0, len = myArray.length; i < len; i++) {
			        if (myArray[i][property] === searchTerm) return i;
			    }
			    return -1;
			}
			
        }
})();