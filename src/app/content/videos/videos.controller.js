(function () {
    'use strict';

    angular.module('youplaylist.videos')
        .controller('VideosController', VideosController);

        function VideosController ($scope, $element, $timeout, $http, $localStorage, $state, TAGS) {

            var vm = this;
            vm.tags = $localStorage.tags;
            
			vm.videos = $localStorage.videos;
			$scope.removeVideo = removeVideo;

			//Supprimer une video de la playlist
			function removeVideo (video) {
                var index = arrayObjectIndexOf(vm.videos, video.id, "id"); // 1
                vm.videos.splice(index, 1);
	        };

	        function arrayObjectIndexOf(myArray, searchTerm, property) {
			    for(var i = 0, len = myArray.length; i < len; i++) {
			        if (myArray[i][property] === searchTerm) return i;
			    }
			    return -1;
			}
			
        }
})();