'use strict';

var config        = require('../config');
var gulp          = require('gulp');
var browserSync = require('browser-sync');

function isOnlyChange(event) {
  return event.type === 'changed';
}

  gulp.task('watch', ['inject'], function () {

    gulp.watch([config.src.root + '/*.html', 'bower.json'], ['inject']);

    // TODO: add a watch for common HTML files and copy to a common dir
    gulp.watch([
      config.src.app + '/**/*.css',
      config.src.app + '/**/*.scss'
    ], function(event) {
      if(isOnlyChange(event)) {
        gulp.start('styles');
      } else {
        gulp.start('inject');
      }
    });

    gulp.watch(config.scripts.src, function(event) {
        if(isOnlyChange(event)) {
            gulp.start('scripts');
        } else {
            gulp.start('inject');
        } 
    });

    gulp.watch(config.src.app + '/**/*.html', function(event) {
      browserSync.reload(event.path);
    });
  });