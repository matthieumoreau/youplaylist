(function() {
'use strict';

    angular.module('youplaylist.core')
        .config(apiConfig);

    function apiConfig(RestangularProvider, ENV) {
        var baseUrls = {
            // @exclude
            'dev':      '',
            // @endexclude
            'stage':    '',        
            'prod':     ''
        };

        // Base:
        RestangularProvider.setBaseUrl(baseUrls[ENV]);
        // RestangularProvider.setDefaultRequestParams({'access_token': 'foo'});
        RestangularProvider.setRequestSuffix('.json');
    }

})();