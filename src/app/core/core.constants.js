(function() {
'use strict';

    angular.module('youplaylist.core')
    
    .constant('ENV', getEnv())
    .constant('TAGS', getTags())

    function getEnv () {
        var env = '/* @echo ENV */';
        // @exclude
        env = 'dev';
        // @endexclude
        return env;
    }


    function getTags () {
        var options = [
            "test", 
           	"electro",
           	"deep house", 
           	"house", 
           	"tropical house",
           	"rock",
           	"rap", 
           	"Hip-hop"
        ];

        return options;
    }

})();
