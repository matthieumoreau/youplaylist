(function () {
'use strict';

    /**
     * ...
     */
    angular.module('youplaylist.videos')
        .run(appRun);
    
    //-------------------------------------------------------------------------
    /* @ngInject */

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'videos',
                config: {
                    templateUrl: 'app/content/videos/videos.html',
                    url: '/videos',
                    controller: 'VideosController',
                    controllerAs: 'vm'
                }
            }
       ];
    }
})();