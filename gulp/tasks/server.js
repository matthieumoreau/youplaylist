'use strict';

var config          = require('../config');
var gulp            = require('gulp');
var browserSync     = require('browser-sync');
var browserSyncSpa  = require('browser-sync-spa');
var util = require('util');

function browserSyncInit(baseDir, browser) {
  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  if(baseDir === config.src.root || (util.isArray(baseDir) && baseDir.indexOf(config.src.root) !== -1)) {
    routes = {
      '/bower_components': 'bower_components'
    };
  }

  var server = {
    baseDir: baseDir,
    routes: routes,
    ghostMode: {
      clicks: true,
      forms: true,
      scroll: true
    }
  };

  browserSync.instance = browserSync.init({
    startPath: '/',
    server: server,
    browser: browser
  });
}

browserSync.use(browserSyncSpa({
  selector: '[ng-app]'
}));

gulp.task('serve', ['watch'], function () {
  browserSyncInit([config.tmp.root + '/serve', config.src.root]);
});

gulp.task('serve:dist', ['build'], function () {
  browserSyncInit(config.dist.root);
});