'use strict';

var config    = require('../config'),
    gulp      = require('gulp'),
    autoprefixer = require('autoprefixer'),
    handleErrors = require('../util/handleErrors'),
    plugins   = require('gulp-load-plugins')(),
    wiredep   = require('wiredep').stream,
    browserSync = require('browser-sync');


gulp.task('styles', function() {

    var injectFiles = gulp.src([
      config.styles.root + '/modules/*.scss',
      config.styles.root + '/components/*.scss',
      config.src.app + '/**/*.scss',
      '!' +  config.styles.root + '/vendor/**/*.scss',
      '!' +  config.styles.root + '/index.scss',
      '!' +  config.styles.root + '/vendor.scss'
    ], { read: false });

    var injectOptions = {
        starttag: '/* inject:imports */',
        endtag: '/* endinject */',
        transform: function (filepath) {
            console.log(filepath);
            return '@import ".' + filepath + '";';
        }
    };

    var injectVendorsFiles = gulp.src([
      config.styles.root + '/vendor/**/*.scss',
      '!' +  config.styles.root + '/vendor/bootstrap/bootstrap/**/*.scss',
      '!' +  config.styles.root + '/index.scss',
      '!' +  config.styles.root + '/vendor.scss'
    ], { read: false });

    var injectVendorsOptions = {
        starttag: '/* inject:importsVendors */',
        endtag: '/* endinject */',
        transform: function (filepath) {
            console.log(filepath);
            return '@import ".' + filepath + '";';
        }
    };

    var indexFilter  = plugins.filter(['index.scss'], {restore: true});
    var vendorFilter  = plugins.filter(['vendor.scss'], {restore: true});

    return gulp.src([
        config.styles.root + '/index.scss',
        config.styles.root + '/vendor.scss'
    ])
    /**
     * Dynamically injects @import statements into the main app.less file, allowing
     * .less files to be placed around the app structure with the component
     * or page they apply to.
     */
        .pipe(indexFilter)
        .pipe(plugins.inject(injectFiles, injectOptions))
        .pipe(indexFilter.restore)
        .pipe(vendorFilter)
        .pipe(wiredep(config.wiredep))
        .pipe(plugins.inject(injectVendorsFiles, injectVendorsOptions))
        .pipe(vendorFilter.restore)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
            .on('error', handleErrors)
        .pipe(plugins.postcss([ autoprefixer({ browsers: ['last 2 version'] }) ]))
            .on('error', handleErrors)
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(config.tmp.serve + '/app/'))
        .pipe(browserSync.reload({ stream: true }));
});













