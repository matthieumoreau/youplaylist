(function () {
    'use strict';
    angular.module('youplaylist', [
        'youplaylist.core',
        'youplaylist.components',
        'youplaylist.content'
    ]);
})();