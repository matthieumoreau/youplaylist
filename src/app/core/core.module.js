(function() {
'use strict';

 /**
 *  @namespace  Core
 */
 
    angular.module('youplaylist.core', [
        /*
         * Angular modules
         */
        'ngAnimate', 'ngSanitize', 'ngTouch', 'ngCookies', 
        /*
         * Our reusable cross app code modules
         */
        'blocks.router',
        /*
         * 3rd Party modules
         */
        'restangular', 'mgcrea.ngStrap', 'ngStorage'
    ]);

})();
